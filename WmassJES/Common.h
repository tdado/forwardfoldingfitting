#pragma once

#include <map>
#include <string>
#include <vector>

class TH1D;
class TH2D;

namespace Common {
    std::string RemoveSubstrings(const std::string& str, const std::vector<std::string>& remove);

    float GetMax(const TH1D* h1, const TH1D* h2);

    double GetMean(const std::vector<double>& v);

    double GetSigma(const std::vector<double>& v);

    double GetMeanGaus(const std::vector<double>& y, const double min, const double max, double& error);

    double GetSigmaGaus(const std::vector<double>& y,
                        const double min,
                        const double max,
                        double& error,
                        const std::string& path,
                        const bool is_jes);

    void AddMapUncertainty(std::map<std::string, std::pair<double,double> >& map,
                           const double& up,
                           const double& down,
                           const std::string& name);

    double SumCategories(const std::map<std::string, std::pair<double,double> >& map,
                         const bool& isUp);

}
