#pragma once

#include "WmassJES/FoldingParameter.h"

#include "TF2.h"
#include "TH1.h"
#include "TH2D.h"
#include "TRandom3.h"

#include "RooAbsData.h"
#include "RooAbsPdf.h"
#include "RooArgList.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"

#include <memory>
#include <vector>

class FFFitter {

public:
    FFFitter();

    ~FFFitter() = default;

    void SetParameterR(const int steps,
                       const double min,
                       const double max);

    void SetParameterS(const int steps,
                       const double min,
                       const double max);

    void SetRandomSeed(const int seed){m_seed = seed;}

    void PrepareWS(const TH1D& asimov, const TH1D& data);

    void FitTemplates(const TH1D& asimov, const TH1D& data, const bool write, const int printLevel = 1);

    void RunPseudoexperiments(const TH1D& asimov,
                              const TH1D& data,
                              const int n);

    const std::vector<double>& GetPEvaluesS() const {return m_PE_s_values;}
    const std::vector<double>& GetPEvaluesR() const {return m_PE_r_values;}

    double GetBestFitS() const;
    double GetBestFitR() const;

    double GetBestFitSError() const;
    double GetBestFitRError() const;

    double GetBestFitSErrorUp() const;
    double GetBestFitRErrorUp() const;

    double GetBestFitSErrorDown() const;
    double GetBestFitRErrorDown() const;

    int GetSsteps() const {return m_param_s->GetSteps();}
    int GetRsteps() const {return m_param_r->GetSteps();}

    double GetDeltaS() const {return m_param_s->GetDelta();}
    double GetDeltaR() const {return m_param_r->GetDelta();}

    bool FitIsOkay() const {return m_fit_is_okay;}

    double GetMinS() const {return m_param_s->GetMin();}
    double GetMinR() const {return m_param_r->GetMin();}

    double GetMaxS() const {return m_param_s->GetMax();}
    double GetMaxR() const {return m_param_r->GetMax();}

    double IndexToValue(const int index, const FoldingParameter::PARAMETER& par) const;

    void SetInputTemplates(const std::vector<std::vector<TH1D> >& templates);

    void PrepareTemplates();

    void SetOutputFolder(const std::string& folder) {m_output_folder = folder;}

    inline void SetDebug(const bool flag) {m_debug = flag;}

    std::unique_ptr<TH2D> Chi2Histo(const TH1D& data, const bool is_real_data) const;

private:

    void Poissonise(TH1D* h, TRandom3* rand) const;

    void ProcessSingleBin(int ibin);

    void TranslateFormula(const TF2& func);

    void SetStartingParameter(const RooAbsPdf* model,
                              const RooAbsData* data,
                              const std::vector<std::pair<std::string, double> >& param_values) const;

    void PrintParametrisation() const;

    std::unique_ptr<FoldingParameter> m_param_r;
    std::unique_ptr<FoldingParameter> m_param_s;

    int m_seed;
    bool m_fit_is_okay;
    std::vector<std::vector<TH1D> > m_input_templates;

    std::vector<double> m_PE_s_values;
    std::vector<double> m_PE_r_values;
    std::string m_output_folder;
    std::vector<std::string> m_fit_formula_per_bin;
    RooArgList m_observables;
    std::unique_ptr<RooWorkspace> m_ws;
    bool m_debug;
    std::unique_ptr<RooFitResult> m_fit_result;
    double m_asimov_integral;

};
