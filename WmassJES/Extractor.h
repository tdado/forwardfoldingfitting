#pragma once

#include "WmassJES/FFFitter.h"

#include "TFile.h"
#include "TH1D.h"

#include <fstream>
#include <map>
#include <memory>
#include <string>
#include <vector>

class TH2D;

class Extractor {

public:
    explicit Extractor();

    ~Extractor() = default;

    void ExtractJES();

private:

};
