#include "WmassJES/Extractor.h"
#include "WmassJES/Common.h"
#include "AtlasUtils/AtlasLabels.h"
#include "AtlasUtils/AtlasStyle.h"

#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLatex.h"
#include "TPad.h"
#include "TSystem.h"

#include <algorithm>
#include <iostream>

Extractor::Extractor() {
    SetAtlasStyle();
}

void Extractor::ExtractJES() {

    FFFitter fitter{};

    // set debug?
    fitter.SetDebug(false);

    // update these values
    fitter.SetParameterS(41, 0.95, 1.05);
    fitter.SetParameterR(41, 0.8, 1.2);

    // set output folder
    const std::string outputFolder = "results";

    gSystem->mkdir(outputFolder.c_str());
    fitter.SetOutputFolder(outputFolder);
    
    // here you need to set the templates!
    const std::vector<std::vector<TH1D> > templates = {};
    fitter.SetInputTemplates(templates);
    fitter.PrepareTemplates();

    // here you need to pass the actual asimov dataset!
    std::unique_ptr<TH1D> asimov = std::make_unique<TH1D>("","", 1, 0, 1);
    fitter.PrepareWS(*asimov, *asimov);
 

    // you can get the vector of chi^2 values with
    // fitter.Chi2Histo(*asimov, false)

    fitter.FitTemplates(*asimov, *asimov, false);

    const double best_s = fitter.GetBestFitS();
    const double best_r = fitter.GetBestFitR();
    std::cout << "\n****************************************************************\n";
    std::cout << "********************* R E S U L T ASIMOV ***********************\n";
    std::cout << "****************************************************************\n\n";
    std::cout << "R = " << best_r << " +" << fitter.GetBestFitRErrorUp() << fitter.GetBestFitRErrorDown() << "\n";
    std::cout << "S = " << best_s << " +" << fitter.GetBestFitSErrorUp() << fitter.GetBestFitSErrorDown() << "\n";
    std::cout << "\n****************************************************************\n\n";

    // you can run pseudoexperiments like this

    // fitter.RunPseudoexperiments(asimov, asimov, 1000);
    // const std::vector<double> s_PE = fitter.GetPEvaluesS();
    // const std::vector<double> r_PE = fitter.GetPEvaluesR();
}
