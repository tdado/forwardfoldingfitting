#include "WmassJES/FFFitter.h"

#include "TCanvas.h"
#include "TGraph2DErrors.h"
#include "TH1.h"
#include "TH2D.h"
#include "TLegend.h"
#include "TMatrixDSym.h"
#include "TSystem.h"

#include "RooAbsReal.h"
#include "RooArgSet.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooFormulaVar.h"
#include "RooMinimizer.h"
#include "RooMultiVarGaussian.h"
#include "RooProdPdf.h"
#include "RooRealVar.h"

#include "AtlasUtils/AtlasLabels.h"
#include "AtlasUtils/AtlasStyle.h"
#include "AtlasUtils/AtlasUtils.h"

#include <iostream>
#include <fstream>
#include <sstream>

FFFitter::FFFitter() :
    m_param_r(nullptr),
    m_param_s(nullptr),
    m_seed(0),
    m_fit_is_okay(true),
    m_debug(false),
    m_asimov_integral(0) {

    RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
    RooMsgService::instance().getStream(1).removeTopic(RooFit::ObjectHandling);

    SetAtlasStyle();
}

void FFFitter::SetParameterR(const int steps,
                             const double min,
                             const double max) {

    if (steps <= 1) {
        std::cerr << "FFFitter::SetParameterR: ERROR Number of steps <= 1!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (min >= max) {
        std::cerr << "FFFitter::SetParameterR: ERROR Min >= max!" << std::endl;
        exit(EXIT_FAILURE);
    }

    m_param_r = std::make_unique<FoldingParameter>(steps, min, max);
}

void FFFitter::SetParameterS(const int steps,
                             const double min,
                             const double max) {

    if (steps <= 1) {
        std::cerr << "FFFitter::SetParameterS: ERROR Number of steps <= 1!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (min >= max) {
        std::cerr << "FFFitter::SetParameterS: ERROR Min >= max!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_param_s = std::make_unique<FoldingParameter>(steps, min, max);
}

void FFFitter::PrepareWS(const TH1D& asimov, const TH1D& data) {
    std::cout << "FFFitter::PrepareWS: Started preparing the RooFit workspace\n";
    m_asimov_integral = asimov.Integral();
    if (m_asimov_integral < 1e-4) {
        std::cerr << "FFFitter::PrepareWS: Asimov Integral() < 1e-4, this is very suspicious!\n";
        m_asimov_integral = 1e-4;
    }

    m_fit_is_okay = false;

    m_ws = std::make_unique<RooWorkspace>("workspace");

    std::size_t nbins = m_fit_formula_per_bin.size();

    // data
    RooArgList dataArgs;
    for (std::size_t ibin = 0; ibin < nbins; ++ibin) {
        const std::string name = "data_bin_" + std::to_string(ibin);
        const double data_content = data.GetBinContent(ibin+1);
        const double asimov_content = asimov.GetBinContent(ibin+1);
        const double strength = std::abs(asimov_content) < 1e-6 ? 0. : data_content/asimov_content;
        RooRealVar value(name.c_str(), name.c_str(), strength, -1, 10);
        dataArgs.addClone(value);
        m_observables.addClone(std::move(value));
    }

    // formulae per bin
    RooArgList parameters;
    RooRealVar jes("JES", "JES", 1., m_param_s->GetMin(), m_param_s->GetMax());
    RooRealVar jer("JER", "JER", 1., 0.2*m_param_r->GetMin(), 5.*m_param_r->GetMax());
    RooRealVar norm("NORM", "NORM", 1., 0.5, 1.5);
    parameters.addClone(jes);
    parameters.addClone(jer);
    parameters.addClone(norm);

    RooArgList formulae;
    for (std::size_t ibin = 0; ibin < nbins; ++ibin) {
        const std::string name = "formula_bin_" + std::to_string(ibin);
        RooFormulaVar formula(name.c_str(), name.c_str(), m_fit_formula_per_bin.at(ibin).c_str(), parameters);
        formulae.addClone(formula);
    }

    TMatrixDSym cov(nbins);
    for (std::size_t i = 0; i < nbins; ++i) {
        for (std::size_t j = 0; j < nbins; ++j) {
            const double data_content = data.GetBinContent(i+1);
            //const double asimov_content = asimov.GetBinContent(i+1);
            const double error = std::abs(data_content) < 1e-6 ? 1. : 1./data_content;
            cov(i,j) = (i == j) ? error : 0.;
        }
    }
    if (m_debug) {
        std::cout << "Printing covariance matrix\n";
        cov.Print();
    }

    RooMultiVarGaussian chi2("chi2", "chi2", dataArgs, formulae, cov);

    m_ws->import(chi2, RooFit::RecycleConflictNodes());

    if (m_debug) {
        m_ws->Print();
    }
    std::cout << "FFFitter::PrepareWS: Finished preparing the RooFit workspace\n\n";
}

void FFFitter::FitTemplates(const TH1D& asimov, const TH1D& data, const bool write, const int printLevel) {
    if (!m_ws) {
        std::cerr << "FFFitter::FitTemplates: WS is nullptr!\n";
        exit(EXIT_FAILURE);
    }
    auto model = m_ws->pdf("chi2");
    if (!model) {
        std::cerr << "Cannot find the PDF to fit from the WS\n";
        exit(EXIT_FAILURE);
    }
    m_fit_is_okay = true;

    const int nbins = data.GetNbinsX();

    // add data
    RooDataSet dataset("data", "data", m_observables);
    for (int ibin = 0; ibin < nbins; ++ibin) {
        const std::string name = "data_bin_" + std::to_string(ibin);
        auto tmp = m_observables.find(name.c_str());
        if (tmp) {
            const double data_content = data.GetBinContent(ibin+1);
            const double asimov_content = asimov.GetBinContent(ibin+1);
            if (std::abs(asimov_content) < 1e-6) {
                *static_cast<RooRealVar*>(tmp) = 1;
            } else {
                *static_cast<RooRealVar*>(tmp) = data_content/asimov_content;
            }
        }
    }

    dataset.add(m_observables);

    if (write) {
        m_ws->import(dataset);
        m_ws->writeToFile((m_output_folder+"/Workspace.root").c_str());
    }

    std::vector<std::pair<std::string, double> > params_starting_values;
    params_starting_values.emplace_back(std::make_pair("JES", 1.));
    params_starting_values.emplace_back(std::make_pair("JER", 1.));
    params_starting_values.emplace_back(std::make_pair("NORM", data.Integral()/m_asimov_integral));

    this->SetStartingParameter(model, &dataset, params_starting_values);

    std::unique_ptr<RooAbsReal> nll(model->createNLL(dataset, RooFit::Offset(kTRUE), RooFit::Optimize(kTRUE)));
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    const TString minimType = ::ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str();
    const TString algorithm = ::ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str();
    const double tol =        ::ROOT::Math::MinimizerOptions::DefaultTolerance();

    RooMinimizer minimizer(*nll);
    minimizer.optimizeConst(2);
    minimizer.setMinimizerType(minimType.Data());
    minimizer.setPrintLevel(printLevel);
    minimizer.setEps(tol);
    int status = minimizer.minimize(minimType.Data(),algorithm.Data());
    if (status != 0) {
        std::cerr << "FFFitter::FitTemplates: The fit failed!\n";
        m_fit_is_okay = false;
        return;
    }

    minimizer.hesse();
    minimizer.minos();
    m_fit_result.reset(minimizer.save());
}

void FFFitter::RunPseudoexperiments(const TH1D& asimov,
                                    const TH1D& data,
                                    const int n) {

    m_PE_s_values.clear();
    m_PE_r_values.clear();

    TRandom3 rand(m_seed);

    std::cout << "FFFitter::RunPseudoexperiments: Started running PE \n";
    /// loop on PE
    for (int ipe = 0; ipe < n; ++ipe) {
        if (ipe % 100 == 0) {
            std::cout << "FFFitter::RunPseudoexperiments: Running PE: " << ipe << " out of " << n << " PEs \n";
        }
        std::unique_ptr<TH1D> data_smeared(static_cast<TH1D*>(data.Clone()));

        /// Smear data
        Poissonise(data_smeared.get(), &rand);

        this->FitTemplates(asimov, *data_smeared, false, -1);

        if (m_fit_is_okay) {
            m_PE_s_values.emplace_back(this->GetBestFitS());
            m_PE_r_values.emplace_back(this->GetBestFitR());
        }
    }
}

double FFFitter::IndexToValue(const int index, const FoldingParameter::PARAMETER& par) const {
    double result(9999);

    if (par == FoldingParameter::PARAMETER::S) {
        result = m_param_s->GetMin() + index*m_param_s->GetDelta();
    } else if (par == FoldingParameter::PARAMETER::R) {
        result = m_param_r->GetMin() + index*m_param_r->GetDelta();
    } else {
        std::cerr << "FFFitter::IndexToValue: ERROR Unknown parameter, returning -1" << std::endl;
        return -1;
    }

    return result;
}

void FFFitter::Poissonise(TH1D* h, TRandom3* rnd) const {
    const int nbins = h->GetNbinsX();

    for (int ibin = 1; ibin <= nbins; ++ibin) {
        const double poissonised = rnd->Poisson(h->GetBinContent(ibin));
        h->SetBinContent(ibin, poissonised);
        h->SetBinError(ibin, std::sqrt(poissonised));
    }
}

void FFFitter::SetInputTemplates(const std::vector<std::vector<TH1D> >& templates) {
    m_input_templates.clear();
    for (const auto& i : templates) {
        std::vector<TH1D> tmp;
        for (const auto& j : i) {
            tmp.emplace_back(*static_cast<TH1D*>(j.Clone()));
        }
        m_input_templates.emplace_back(std::move(tmp));
    }
}

void FFFitter::PrepareTemplates() {
    std::cout << "FFFitter::PrepareTemplates: Started running the per bin parametrisation\n";
    std::size_t size = m_input_templates.size();

    if (size == 0) {
        std::cerr << "FFFitter::PrepareTemplates: No input templates provided\n";
        exit(EXIT_FAILURE);
    }

    gSystem->mkdir((m_output_folder+"/Parametrisation/").c_str());
    int bin_size = m_input_templates.at(0).at(0).GetNbinsX();
    m_fit_formula_per_bin.clear();
    for (int ibin = 1; ibin <= bin_size; ++ibin) {
        this->ProcessSingleBin(ibin);
    }
    this->PrintParametrisation();
    std::cout << "FFFitter::PrepareTemplates: Finished running the per bin parametrisation\n";
}

void FFFitter::ProcessSingleBin(int ibin) {
    std::size_t sizeS = m_input_templates.size();
    std::size_t sizeR = m_input_templates.at(0).size();

    const size_t central_s = m_param_s->GetSteps()/2 + 1;
    const size_t central_r = m_param_r->GetSteps()/2 + 1;

    int n(0);
    TGraph2DErrors graph;
    for (std::size_t is = 0; is < sizeS; ++is) {
        for (std::size_t ir = 0; ir < sizeR; ++ir) {
            const double s = m_param_s->GetMin() + is * m_param_s->GetDelta();
            const double r = m_param_r->GetMin() + ir * m_param_r->GetDelta();
            const double content = m_input_templates.at(is).at(ir).GetBinContent(ibin);
            const double content_central = m_input_templates.at(central_s).at(central_r).GetBinContent(ibin);
            const double uncertainty_central = m_input_templates.at(is).at(ir).GetBinError(ibin)/content_central;
            graph.SetPoint(n, s, r, content/content_central);
            graph.SetPointError(n, 0, 0, uncertainty_central);
            ++n;
        }
    }
    // run the 2D fit
    TF2 func("func","[0]*x*x + [1]*y*y + [2]*x*y + [3]*x + [4]*y + [5]", m_param_s->GetMin(), m_param_s->GetMax(), m_param_r->GetMin(), m_param_r->GetMax());
    func.SetLineColor(kRed);
    if (m_debug) {
        std::cout << "Fitting bin: " << ibin << "\n";
        graph.Fit(&func, "R");
    } else {
        graph.Fit(&func, "RQ");
    }

    TCanvas c("","",800,600);
    func.GetXaxis()->SetTitle("S (JES) parameter");
    func.GetXaxis()->SetTitleOffset(1.3*func.GetXaxis()->GetTitleOffset());
    func.GetYaxis()->SetTitle("R (JER) parameter");
    func.GetYaxis()->SetTitleOffset(1.4*func.GetYaxis()->GetTitleOffset());
    func.GetZaxis()->SetTitle("Yield/Nominal yield");
    func.GetZaxis()->SetTitleOffset(2.2*func.GetZaxis()->GetTitleOffset());
    func.Draw("surf1");
    graph.Draw("surf2 same");

    ATLASLabel(0.1,0.9, "Internal");
    myText(0.1,0.85, 1, ("Bin " + std::to_string(ibin)).c_str());

    TLegend leg(0.01,0.01, 0.3, 0.15);
    leg.SetFillStyle(0);
    leg.SetBorderSize(0);
    leg.AddEntry(&graph, "Template histogram", "lf");
    leg.AddEntry(&func, "Fit function", "lf");
    leg.Draw("same");

    this->TranslateFormula(func);

    const std::string name = m_output_folder + "/Parametrisation/Bin_"+std::to_string(ibin)+"_parametrisation.png";
    c.Print(name.c_str());
}

void FFFitter::TranslateFormula(const TF2& func) {
    std::stringstream ss;
    ss << std::scientific;

    ss << "NORM*(" << func.GetParameter(0) << "*JES*JES + " << func.GetParameter(1) << "*JER*JER + " << func.GetParameter(2) << "*JES*JER + " << func.GetParameter(3) << "*JES + ";
    ss << func.GetParameter(4) << "*JER + " << func.GetParameter(5) << ")";

    m_fit_formula_per_bin.emplace_back(ss.str());
}

double FFFitter::GetBestFitS() const {
    if (!m_fit_result) {
        std::cerr << "FFFitter::GetBestFitS: Fit results not found! Returning -1\n";
        return -1;
    }

    for (const auto& ipar : m_fit_result->floatParsFinal()) {
        const std::string name = ipar->GetName();
        if (name != "JES") continue;

        const auto* const var = static_cast<RooRealVar*>(ipar);
        return var->getVal();
    }

    return -1;
}

double FFFitter::GetBestFitR() const {
    if (!m_fit_result) {
        std::cerr << "FFFitter::GetBestFitR: Fit results not found! Returning -1\n";
        return -1;
    }

    for (const auto& ipar : m_fit_result->floatParsFinal()) {
        const std::string name = ipar->GetName();
        if (name != "JER") continue;

        const auto* const var = static_cast<RooRealVar*>(ipar);
        return var->getVal();
    }

    return -1;
}

double FFFitter::GetBestFitSError() const {
    if (!m_fit_result) {
        std::cerr << "FFFitter::GetBestFitSError: Fit results not found! Returning -1\n";
        return -1;
    }

    for (const auto& ipar : m_fit_result->floatParsFinal()) {
        const std::string name = ipar->GetName();
        if (name != "JES") continue;

        const auto* const var = static_cast<RooRealVar*>(ipar);
        return var->getError();
    }

    return -1;
}

double FFFitter::GetBestFitRError() const {
    if (!m_fit_result) {
        std::cerr << "FFFitter::GetBestFitRError: Fit results not found! Returning -1\n";
        return -1;
    }

    for (const auto& ipar : m_fit_result->floatParsFinal()) {
        const std::string name = ipar->GetName();
        if (name != "JER") continue;

        const auto* const var = static_cast<RooRealVar*>(ipar);
        return var->getError();
    }

    return -1;
}

double FFFitter::GetBestFitSErrorUp() const {
    if (!m_fit_result) {
        std::cerr << "FFFitter::GetBestFitSErrorUp: Fit results not found! Returning -1\n";
        return -1;
    }

    for (const auto& ipar : m_fit_result->floatParsFinal()) {
        const std::string name = ipar->GetName();
        if (name != "JES") continue;

        const auto* const var = static_cast<RooRealVar*>(ipar);
        return var->getErrorHi();
    }

    return -1;
}

double FFFitter::GetBestFitRErrorUp() const {
    if (!m_fit_result) {
        std::cerr << "FFFitter::GetBestFitRErrorUp: Fit results not found! Returning -1\n";
        return -1;
    }

    for (const auto& ipar : m_fit_result->floatParsFinal()) {
        const std::string name = ipar->GetName();
        if (name != "JER") continue;

        const auto* const var = static_cast<RooRealVar*>(ipar);
        return var->getErrorHi();
    }

    return -1;
}

double FFFitter::GetBestFitSErrorDown() const {
    if (!m_fit_result) {
        std::cerr << "FFFitter::GetBestFitSErrorDown: Fit results not found! Returning -1\n";
        return -1;
    }

    for (const auto& ipar : m_fit_result->floatParsFinal()) {
        const std::string name = ipar->GetName();
        if (name != "JES") continue;

        const auto* const var = static_cast<RooRealVar*>(ipar);
        return var->getErrorLo();
    }

    return -1;
}

double FFFitter::GetBestFitRErrorDown() const {
    if (!m_fit_result) {
        std::cerr << "FFFitter::GetBestFitRErrorDown: Fit results not found! Returning -1\n";
        return -1;
    }

    for (const auto& ipar : m_fit_result->floatParsFinal()) {
        const std::string name = ipar->GetName();
        if (name != "JER") continue;

        const auto* const var = static_cast<RooRealVar*>(ipar);
        return var->getErrorLo();
    }

    return -1;
}

void FFFitter::SetStartingParameter(const RooAbsPdf* model, const RooAbsData* data, const std::vector<std::pair<std::string, double> >& param_values) const {
    const auto* params = model->getParameters(data);
    for (auto iparam : *params) {
        const std::string name = iparam->GetName();
        auto it = std::find_if(param_values.begin(), param_values.end(), [&name](const auto& element){return element.first == name;});
        if (it != param_values.end()) {
            if (m_debug) {
                std::cout << "FFFitter::SetStartingParameter: Setting parameter " << name << " to: " << it->second << "\n";
            }
            static_cast<RooRealVar*>(iparam)->setVal(it->second);
        }
    }
}

void FFFitter::PrintParametrisation() const {
    if (m_fit_formula_per_bin.empty()) {
        std::cerr << "FFFitter::PrintParametrisation: Fit results per bin are empty!\n";
        return;
    }

    std::ofstream out(m_output_folder+"/Parametrisation/Formulae.txt");

    if (!out.is_open() || !out.good()) {
        std::cerr << "FFFitter::PrintParametrisation: Cannot open file at: " << m_output_folder+"/Parametrisation/Formulae.txt" << "\n";
        return;
    }

    for (std::size_t i = 0; i < m_fit_formula_per_bin.size(); ++i) {
        out << i+1 << "\t" << m_fit_formula_per_bin.at(i) << "\n";
    }

    out.close();
}

std::unique_ptr<TH2D> FFFitter::Chi2Histo(const TH1D& data, const bool is_real_data) const {
    if (m_input_templates.empty()) {
        std::cerr << "FFFitter::Chi2Histo: Input templates are empty!\n";
        return nullptr;
    }

    const std::size_t nx = m_input_templates.size();
    const std::size_t ny = m_input_templates.at(0).size();

    std::unique_ptr<TH2D> result = std::make_unique<TH2D>("","", nx, m_param_s->GetMin(), m_param_s->GetMax(), ny, m_param_r->GetMin(), m_param_r->GetMax());
    for (std::size_t ibin = 0; ibin < nx; ++ibin) {
        for (std::size_t jbin = 0; jbin < ny; ++jbin) {
            double chi2(0);
            if (is_real_data) {
                chi2 = m_input_templates.at(ibin).at(jbin).Chi2Test(&data, "WU CHI2");
            } else {
                chi2 = m_input_templates.at(ibin).at(jbin).Chi2Test(&data, "WW CHI2");
            }
            result->SetBinContent(ibin+1, jbin+1, chi2);
            result->SetBinError(ibin+1, jbin+1, 0.);
        }
    }

    return result;
}