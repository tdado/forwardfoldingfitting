#include "WmassJES/Common.h"
#include "AtlasUtils/AtlasLabels.h"

#include "TCanvas.h"
#include "TF1.h"
#include "TH2D.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLorentzVector.h"
#include "TString.h"

#include <algorithm>
#include <iostream>

std::string Common::RemoveSubstrings(const std::string& str, const std::vector<std::string>& remove) {
    std::string result = str;
    result.erase(std::remove(result.begin(), result.end(), '_'), result.end());

    for (const auto& i : remove) {
        auto itr = result.find(i.c_str());
        if (itr == std::string::npos) continue;

        const auto size = i.size();
        result.erase(itr, size);
    }

    return result;
}

double Common::GetMean(const std::vector<double>& v) {
    double result(0);

    for (const auto& i : v) {
        result+= i;
    }

    result = static_cast<double>(result/v.size());

    return result;
}

double Common::GetSigma(const std::vector<double>& v) {
    if (v.size() < 2) {
        std::cerr << "Common::GetSigma: Size of vector < 2. Returning -1.\n";
        return -1;
    }

    const double mean = Common::GetMean(v);

    double result(0);
    for (const auto& i : v) {
        result+= (i - mean)*(i - mean);
    }

    result = static_cast<double>(result/(v.size() - 1));
    return std::sqrt(result);
}

double Common::GetMeanGaus(const std::vector<double>& y, const double min, const double max, double& error) {

    TH1D h("","", 30, min, max);

    for (const auto& i : y) {
        h.Fill(i);
    }

    TF1 func("func","gaus", min, max);
    func.SetParameter(0, h.GetMaximum());
    func.SetParameter(1,1);
    func.SetParameter(2,0.01);

    const Int_t converge = h.Fit(&func,"Q");
    if (converge != 0) {
        std::cerr << "Common::GetMeanGaus: Fit didnt converge, returning 0\n";
        return 0;
    }

    if (func.GetChisquare() > 3) {
        std::cout << "Common::GetMeanGaus: Gaussian fit has chi^2/NDF = " << func.GetChisquare() << ", the result is probably nonsense\n";
    }

    error = func.GetParError(1);

    return func.GetParameter(1);
}

double Common::GetSigmaGaus(const std::vector<double>& y,
                            const double min,
                            const double max,
                            double& error,
                            const std::string& path,
                            const bool is_jes) {

    TH1D h("", "", 30, min, max);
    for (const auto& i : y) {
        h.Fill(i);
    }

    TF1 func("func","gaus", min, max);
    func.SetParameter(0, h.GetMaximum());
    func.SetParameter(1,1);
    func.SetParameter(2,0.001);

    const Int_t converge = h.Fit(&func,"Q");
    if (converge != 0) {
        std::cerr << "Common::GetSigmaGaus: Fit didnt converge, returning -1\n";
        return -1;
    }

    if (func.GetChisquare() > 3) {
        std::cout << "Common::GetSigmaGaus: Gaussian fit has chi^2/NDF " << func.GetChisquare() << ", the result is probably nonsense\n";
    }

    error = func.GetParError(2);
    const double mean = func.GetParameter(1);
    const double sigma = func.GetParameter(2);
    const double mean_error = func.GetParError(1);

    TCanvas c("","", 800,600);
    h.SetMaximum(1.3*h.GetMaximum());
    if (is_jes) {
        h.GetXaxis()->SetTitle("S (JES) value");
    } else {
        h.GetXaxis()->SetTitle("R (JER) value");
    }
    h.GetYaxis()->SetTitle("Cental value per pseudoexperiment");
    h.Draw("HIST");
    func.SetLineColor(kRed);
    func.Draw("L same");

    TLegend leg(0.7, 0.7, 0.9, 0.9);
    leg.SetBorderSize(0);
    leg.SetFillStyle(0);
    leg.AddEntry(&h, "Pseudoexperiments", "f");
    leg.AddEntry(&func, "Gaussian fit", "l");
    leg.Draw("same");

    ATLASLabel(0.2, 0.9, "Internal");

    TLatex l1;
    l1.SetNDC();
    l1.DrawLatex(0.2, 0.85, Form("Mean (Gauss): %.3f   #pm %.4f", mean, mean_error));
    l1.DrawLatex(0.2, 0.8, Form("Sigma (Gauss): %.4f #pm %.4f", sigma, error));

    c.Print((path+".png").c_str());

    return func.GetParameter(2);
}

void Common::AddMapUncertainty(std::map<std::string, std::pair<double,double> >& map,
                               const double& up,
                               const double& down,
                               const std::string& name) {

    auto itr = map.find(name);
    if (up >= 0 && down <= 0) {
        if (itr == map.end()) {
            map[name] = std::make_pair(up, down);
        } else {
            itr->second.first  = std::hypot(itr->second.first, up);
            itr->second.second = -std::hypot(itr->second.second, down);
        }
    } else if (up <= 0 && down >= 0) {
        if (itr == map.end()) {
            map[name] = std::make_pair(down, up);
        } else {
            itr->second.first  = std::hypot(itr->second.first, down);
            itr->second.second = -std::hypot(itr->second.second, up);
        }
    } else if (up >= 0 && down >= 0) {
        if (itr == map.end()) {
            map[name] = std::make_pair(up, 0);
        } else {
            itr->second.first = std::hypot(itr->second.first, up);
        }
    } else if (up <= 0 && down <= 0) {
        if (itr == map.end()) {
            map[name] = std::make_pair(0, down);
        } else {
            itr->second.second = -std::hypot(itr->second.second, up);
        }
    } else {
        std::cerr << "Common::AddMapUncertainty: Weird composition of uncertainties" << std::endl;
        exit(EXIT_FAILURE);
    }
}

double Common::SumCategories(const std::map<std::string, std::pair<double,double> >& map,
                             const bool& isUp) {

    double sum(0);
    for (const auto& itr : map) {
        sum += isUp ? itr.second.first*itr.second.first : itr.second.second*itr.second.second;
    }

    return std::sqrt(sum);
}

