#include "WmassJES/Extractor.h"

#include "TError.h"

#include <iostream>
#include <string>

int main(int /*argc*/, char** /*argv*/) {
    
    Extractor extractor{};

    extractor.ExtractJES();

    return 0;
}
